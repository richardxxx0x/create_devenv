sudo apt-get install -y curl ssh vim git git-flow zsh tmux libffi-dev libssl-dev libfreetype6-dev libjpeg-dev python-dev python-setuptools libmysqlclient-dev mysql-server-5.6 nodejs npm
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo npm install bower -g
sudo easy_install pip
cd ~
mkdir .pip;cd .pip;touch pip.conf
echo -e '[global]\nindex-url = https://pypi.mirrors.ustc.edu.cn/simple' > ~/.pip/pip.conf
sudo pip install virtualenv virtualenvwrapper
git clone https://github.com/robbyrussell/oh-my-zsh.git
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.zshrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.zshrc
sudo chsh -s `which zsh`
